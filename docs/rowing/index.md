---
layout: default
title: Rowing
nav_order: 5
---

# Rowing
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


## The Pete Plan

[Source of the Pete Plan](https://thepeteplan.wordpress.com/the-pete-plan/). Copied just as a backup.


### Week 1

| day | workout                     |
|:---:|-----------------------------|
|  1  | 8 x 500m / 3min30 rest      |
|  2  | Steady distance (~8 to 15k) |
|  3  | 5 x 1500m / 5min rest       |
|  4  | Steady distance (~8 to 15k) |
|  5  | Hard distance (~5k+)        |
|  6  | Steady distance (~8 to 15k) |
|  7  | Rest                        |

### Week 2

| day | workout                                                            |
|:---:|--------------------------------------------------------------------|
|  1  | 250m, 500m, 750m, 1k, 750m, 500m, 250m / 1min30 rest per 250m work |
|  2  | Steady distance (~8 to 15k)                                        |
|  3  | 4 x 2000m / 5min rest                                              |
|  4  | Steady distance (~8 to 15k)                                        |
|  5  | Hard distance (~5k+)                                               |
|  6  | Steady distance (~8 to 15k)                                        |
|  7  | Rest                                                               |

### Week 3

| day | workout                     |
|:---:|-----------------------------|
|  1  | 4 x 1000m / 5min rest       |
|  2  | Steady distance (~8 to 15k) |
|  3  | 3k, 2.5k, 2k / 5min rest    |
|  4  | Steady distance (~8 to 15k) |
|  5  | Hard distance (~5k+)        |
|  6  | Steady distance (~8 to 15k) |
|  7  | Rest                        |

### Pacing

For both speed intervals and endurance intervals, the target pace is the average pace of the previous session. In the final rep, go as fast as possible.

For steady distance sessions, go 22-25 strokes per minute, at least 10 seconds slower than the endurance interval sessions. If in doubt, go slower!


## ZOAR fitness

- [Drag factor test](https://www.zoarfitness.com/post/drag-factor-test/)
- [About rowing](https://www.zoarfitness.com/movements/rowing/)

---
layout: default
title: Assessment
nav_order: 97
parent: Mobility
---

# Assessing ...

## Hips

Are your hip flexors tight or weak?

**Athlean-X** has some tests.

{% youtube "https://www.youtube.com/watch?v=KCADorwJ6Ik" %}

---
layout: default
title: Ankles
parent: Mobility
nav_order: 1
---

# Ankles
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


## Squat University
### Banded Joint Mobilization

{% youtube "https://www.youtube.com/watch?v=ILSbK8RnGdI" %}

### Fix Your Squat: Part 1 - Ankle Mobility for Squatting

{% youtube "https://www.youtube.com/watch?v=-c2l_NBV234" %}


## Athlean-X

### Ankle Sprain Fix and Prevention

{% youtube "https://www.youtube.com/watch?v=pzy289wmsKQ" %}

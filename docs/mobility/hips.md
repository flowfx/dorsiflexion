---
layout: default
title: Hips
parent: Mobility
nav_order: 2
---

# Hips
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


## Shin box

5 hip mobility exercises by Mark Wildman.

1. Windshield wiper
2. Shin box
3. Shin box arm to knee
4. Shin box shoulder to knee
5. 4 count

{% youtube "https://www.youtube.com/watch?v=3qFYxMXCC3A&t=287s" %}

## 4 Count

{% youtube "https://www.youtube.com/watch?v=gvQ206hJgaA&t=245s" %}

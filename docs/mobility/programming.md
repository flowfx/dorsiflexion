---
layout: default
title: Programs
nav_order: 98
parent: Mobility
---

# Programs & routines

## A: Ankle focus

1. **Warmup:** [Shin box routing](hips.html#shin-box) (5 min).
2. 5 min lacrosse ball on shin (inner/outer), calves, foot.
3. 3 x 1 min stretching of **soleus** (6+ min).
4. 3 x 1 min stretching of **gastrocnemius** (6+ min).

~28 min total.

## B: Hip focus

1. Warmup w/ Movement Prep a la *Core Performance*
2. 5 min foam rolling adductors, gluteus.
3. 3 x 1 min stretching of inner hip rotator: 90/90 stretch.
4. 3 x 1 min stretching of hip flexor & quadrizps (?): sofa stretch.

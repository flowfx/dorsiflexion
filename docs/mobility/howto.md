---
layout: default
title: Howto
nav_order: 99
parent: Mobility
---

# How to stretch

##  Yiannis Christoulas

### Get Flexible by Science

- Time spent stretching per week >> Time spent stretching per session
- more than 10 minutes stretching per week has no extra benefit
- 6x / week: 3 sets of 30 seconds = 9 min
- **3x / week: 3 sets of 60 seconds** = 9 min
- low intensity >> moderate intensity stretching
- warm up!
- breath calmly
- take a break every 6-8 weeks

{% youtube "https://www.youtube.com/watch?v=Qf8dmyg2jck" %}

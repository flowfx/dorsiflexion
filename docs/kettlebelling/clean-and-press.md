---
layout: default
title: The Clean & Press
parent: Kettlebelling
---

# The Clean & Press
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Mark Wildman

### A Primer
> *The single-arm clean & press is fundamental, and you should do a lot of them!*

**Pointers:**

- Point both feet straight ahead.
- Point thumb back.
- L to the L.
- Stand up with your hips, extend your hips all the way.
- Move the kettlebell around your body, not your body around the kettlebell.
- Squeeze the glutes.
- Overhead lockout!
- t.b.c.

{% youtube "https://www.youtube.com/watch?v=48qvCvJJr8Y" %}

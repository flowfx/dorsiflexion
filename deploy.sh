#!/bin/sh

bundle exec jekyll build
rsync -rav --delete _site/ flowfx@ariel.uberspace.de:/var/www/virtual/flowfx/html/dorsiflexion.flowfx.de/

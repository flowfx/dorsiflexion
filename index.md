---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: Home
nav_order: 1
---

# Repository of stuff
{: .fs-9 }

Just the Docs gives your documentation a jumpstart with a responsive Jekyll theme that is easily customizable and hosted on GitHub Pages.
{: .fs-6 .fw-300 }

[Check the docs of just-the-docs](https://pmarsceill.github.io/just-the-docs/){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }
